FROM ubuntu:18.04

# install docker-ce
RUN apt-get --quiet=2 update
RUN apt-get --quiet=2 install --no-install-recommends \
    ca-certificates iptables libdevmapper1.02.1 libseccomp2 libltdl7 wget
RUN wget --no-verbose \
  --output-document=/tmp/docker-ce_18.05.0~ce~3-0~ubuntu_amd64.deb \
  https://download.docker.com/linux/ubuntu/dists/bionic/pool/edge/amd64/docker-ce_18.05.0~ce~3-0~ubuntu_amd64.deb
RUN dpkg --install /tmp/docker-ce_18.05.0~ce~3-0~ubuntu_amd64.deb

# install tools
RUN apt-get --quiet=2 install --no-install-recommends \
    curl \
    dnsutils \
    git \
    httpie \
    iproute2 \
    iputils-ping \
    less \
    make \
    openssh-client \
    python-msgpack \
    python-yaml \
    sqlite3 \
    strace \
    sudo
