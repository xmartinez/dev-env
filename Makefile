.PHONY: image test

image:
	docker build --tag=dev-env --file=./Dockerfile .

test:
	./test.py
