#!/usr/bin/python2.7

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import getpass
import grp
import json
import os
import sys
from pprint import pprint
from subprocess import CalledProcessError, call, check_call, check_output

RBROOT = os.path.abspath(os.path.dirname(__file__))


def assert_eq(expected, actual):
    if actual != expected:
        raise AssertionError('expected {!r}, got {!r}'.format(
            expected, actual))


def run_test():
    try:
        os.environ['PAGER'] = '/bin/cat'
        result = check_output(['./devenv.py', 'run', './check-env'])
    except CalledProcessError as exc:
        print(exc.output)
        raise
    result = json.loads(result)
    pprint(result)

    assert_eq(os.getcwd(), result['cwd'])
    assert_eq(os.geteuid(), result['euid'])
    assert_eq(os.getegid(), result['egid'])
    assert_eq(getpass.getuser(), result['user'])
    assert_eq(grp.getgrgid(os.getegid()).gr_name, result['group'])
    assert_eq(0, result['docker_exit_status'])

    env = result['env']
    assert_eq(RBROOT, env.get('RBROOT'))
    assert_eq(os.path.join(RBROOT, '.build'), env.get('RBBUILD'))


def main():
    check_call(['./devenv.py', 'stop'])
    run_test()  # new container
    run_test()  # container already running


if __name__ == '__main__':
    main()
