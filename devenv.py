#!/usr/bin/python2.7
"""Development environment container management script."""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import argparse
import getpass
import grp
import json
import os
import os.path
import sys
from subprocess import call, check_call, check_output, PIPE

image_name = 'dev-env'
image_tag = 'latest'
image = '{}:{}'.format(image_name, image_tag)

RBROOT = os.path.abspath(os.path.dirname(__file__))

default_docker_gid = 999

bind_dirs = [
    (RBROOT, RBROOT),
    ('~/test', '~/test'),
    ('~/.config/dev-env', '~'),
]

if 'RBDEV_ENV_IMAGE' in os.environ:
    # Allow image overriding (e.g., for testing local dev-env images)
    image = os.environ['RBDEV_ENV_IMAGE']
    image_hash = image.split(':')[1]
    image_tag = image_hash[:12]

rbenv = '{}_{}'.format(image_name, image_tag)
if 'CI_JOB_ID' in os.environ:
    rbenv = '{}_{}'.format(rbenv, os.environ['CI_JOB_ID'])

container = rbenv
hostname = rbenv
network = rbenv


def mkdirp(path):
    if not os.path.exists(path):
        os.makedirs(path)


def bind_dir_map(path, bind_dirs):
    path = os.path.abspath(path)
    for src, dest in bind_dirs:
        src = os.path.expanduser(src)
        if path == src:
            return dest
        elif path.startswith(src + '/'):
            return dest + path[len(src):]


def chdir(path):
    log("Entering directory '{}'".format(path))
    mkdirp(path)
    os.chdir(path)


def log(message):
    print(
        "{}: {}".format(os.path.basename(sys.argv[0]), message),
        file=sys.stderr)


def docker_ensure_network(network):
    out = check_output([
        'docker',
        'network',
        'ls',
        '--format={{.Name}}',
        '--filter=name=^{}$'.format(network),
    ])
    if not out:
        check_call(['docker', 'network', 'create', network], stdout=PIPE)


def docker_get_gid():
    try:
        return grp.getgrnam('docker').gr_gid
    except KeyError:
        # We are likely running on macOS. Use default docker gid.
        return default_docker_gid


def docker_status(container):
    status = check_output([
        'docker',
        'ps',
        '--all',
        '--filter=name=^/{}$'.format(container),
        '--format={{.Status}}',
    ])
    if status:
        return status.split()[0].lower()


def devenv_run(args):
    # Fast-path: already inside dev-env container
    if os.environ.get('RBENV') == rbenv:
        os.execvp(args.command[0], args.command)

    # Ensure that dev-env container is running.
    created = devenv_start()
    if created:
        devenv_setup()

    # Environment variables.
    env = dict(
        RBBUILD=os.path.join(RBROOT, '.build'),
        RBENV=rbenv,
        RBENV_NETWORK=network,
        RBROOT=RBROOT,
    )

    # Pass-through environment variable names.
    env_pass = [
        'HOME',
        'PAGER',
        'RBDEV_ENV_IMAGE',
        'SHELL',
        'SSH_AUTH_SOCK',
    ]

    # docker-exec options.
    opts = []

    # Add rbcode bin to PATH
    path = [
        os.path.join(RBROOT, 'bin'),
        '/usr/local/sbin',
        '/usr/local/bin',
        '/usr/sbin',
        '/usr/bin',
        '/sbin',
        '/bin',
    ]
    env.update(PATH=':'.join(path))

    # Ensure that we have sane locale settings (otherwise, python click-based
    # tools such as pipenv will fail).
    env.update(
        LC_ALL='C.UTF-8',
        LANG='C.UTF-8',
    )

    # Pass CI env vars if running in a GitLab CI environment.
    env_pass.append('CI')
    env_pass.extend(var for var in os.environ if var.startswith('CI_'))

    # Allocate a pseudo-tty if stdin and stdout are a tty.
    if sys.stdin.isatty() and sys.stdout.isatty():
        opts.append('--tty')

        # Set COLUMNS and LINES to work around a bug in docker-exec (see
        # https://github.com/moby/moby/issues/35407).
        env.update(
            COLUMNS=check_output(['tput', 'cols']).strip(),
            LINES=check_output(['tput', 'lines']).strip(),
        )

    # Run as current effective user and group.
    opts.append('--user={}:{}'.format(os.geteuid(), os.getegid()))

    # Try to preserve working directory.
    if bind_dir_map(os.getcwd(), bind_dirs) is None:
        chdir(RBROOT)
    cwd = bind_dir_map(os.getcwd(), bind_dirs)
    opts.append('--workdir={}'.format(cwd))

    # run command in container
    env.update(
        (name, os.environ[name]) for name in env_pass if name in os.environ)
    opts.extend(
        '--env={}={}'.format(name, value) for name, value in env.items())
    exec_cmd = ['docker', 'exec', '-i'] + opts + [container] + args.command
    os.execvp(exec_cmd[0], exec_cmd)


def devenv_setup():
    pass


def devenv_start():
    status = docker_status(container)
    if status == 'up':
        # Already running.
        return False
    elif status is not None:
        # Container stopped.  Start it.
        check_call(['docker', 'start', container])
        return False

    # docker-run options.
    run_opts = []

    # Bind mounts: [(src, dest), ...]
    bind_mounts = []

    # Allow strace to be used
    run_opts.append('--cap-add=SYS_PTRACE')

    # Bind mount config and code dirs.
    bind_mounts.extend((os.path.expanduser(src), os.path.expanduser(dest))
                       for src, dest in bind_dirs)
    for src, _ in bind_dirs:
        mkdirp(os.path.expanduser(src))

    # Allow accessing host ssh-agent from inside the container.
    if os.environ.get('SSH_AUTH_SOCK'):
        ssh_auth_sock = os.environ['SSH_AUTH_SOCK']
        bind_mounts.append((ssh_auth_sock, ssh_auth_sock))

    # Allow docker access from inside container.
    run_opts.append('--group-add={}'.format(docker_get_gid()))
    bind_mounts.append(('/run/docker.sock', '/run/docker.sock'))

    # Run as current effective user and group.
    run_opts.append('--user={}:{}'.format(os.geteuid(), os.getegid()))

    # Create network.
    docker_ensure_network(network)

    # Start new container.
    run_opts.extend(
        '--mount=type=bind,source={},destination={}'.format(src, dest)
        for src, dest in bind_mounts)
    run_cmd = [
        'docker',
        'run',
        '--detach',
        '--init',
        '--entrypoint=',
        '--hostname={}'.format(hostname),
        '--name={}'.format(container),
        '--network={}'.format(network),
        '--workdir=/',
    ]
    check_call(run_cmd + run_opts + [image, 'sleep', 'inf'], stdout=PIPE)

    # Set up container.
    setup_args = dict(
        docker_gid=docker_get_gid(),
        gid=os.getegid(),
        group=grp.getgrgid(os.getegid()).gr_name,
        home=os.environ['HOME'],
        uid=os.geteuid(),
        user=getpass.getuser(),
    )
    setup_cmd = [
        'docker', 'exec', '-i', '--user=0', container,
        os.path.join(RBROOT, 'setup'),
        json.dumps(setup_args)
    ]
    check_call(setup_cmd)

    # Container created.
    return True


def devenv_stop():
    call(['docker', 'rm', '--force', container])
    for svc in ['db-pg-9.6', 'gateway', 'pubsub']:
        call(['docker', 'network', 'disconnect', '--force', network, svc])
    call(['docker', 'network', 'rm', network])


def parse_args(args):
    parser = argparse.ArgumentParser()
    cmds = parser.add_subparsers(dest='cmd')

    sub_cmd = cmds.add_parser('run')
    sub_cmd.add_argument('command', nargs=argparse.REMAINDER)

    sub_cmd = cmds.add_parser('start')
    sub_cmd = cmds.add_parser('stop')

    return parser.parse_args(args)


def main():
    args = parse_args(sys.argv[1:])
    if args.cmd == 'run':
        devenv_run(args)
    elif args.cmd == 'start':
        devenv_start()
    elif args.cmd == 'stop':
        devenv_stop()


if __name__ == '__main__':
    main()
